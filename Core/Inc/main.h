/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
typedef struct CMD {
  uint8_t CMD_NUM;
  uint8_t CMD_ARGS[10];
} CMD_t;

typedef struct MSG {
  char buffer[41];
} MSG_t;

typedef struct ECOMP {
  uint16_t voltage;
  uint16_t current;
  uint16_t rpm;
  uint16_t temp;
  uint8_t status;
} ECOMP_t;
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BAT_ADC_EN_Pin GPIO_PIN_13
#define BAT_ADC_EN_GPIO_Port GPIOC
#define MOTOR_VENT_IN2_Pin GPIO_PIN_0
#define MOTOR_VENT_IN2_GPIO_Port GPIOC
#define MOTOR_VENT_IN1_Pin GPIO_PIN_1
#define MOTOR_VENT_IN1_GPIO_Port GPIOC
#define MOTOR_FOOT_IN2_Pin GPIO_PIN_2
#define MOTOR_FOOT_IN2_GPIO_Port GPIOC
#define MOTOR_FOOT_IN1_Pin GPIO_PIN_3
#define MOTOR_FOOT_IN1_GPIO_Port GPIOC
#define IGN2_INT_Pin GPIO_PIN_0
#define IGN2_INT_GPIO_Port GPIOA
#define ADC_HAPT_SENSOR_Pin GPIO_PIN_1
#define ADC_HAPT_SENSOR_GPIO_Port GPIOA
#define ADC_AMB_SENSOR_Pin GPIO_PIN_2
#define ADC_AMB_SENSOR_GPIO_Port GPIOA
#define ADC_DAT_VENT_SENSOR_Pin GPIO_PIN_3
#define ADC_DAT_VENT_SENSOR_GPIO_Port GPIOA
#define ADC_LEFT_SUN_SENSOR_Pin GPIO_PIN_4
#define ADC_LEFT_SUN_SENSOR_GPIO_Port GPIOA
#define ADC_RIGHT_SUN_SENSOR_Pin GPIO_PIN_5
#define ADC_RIGHT_SUN_SENSOR_GPIO_Port GPIOA
#define ADC_BLWR_MOTOR__Pin GPIO_PIN_6
#define ADC_BLWR_MOTOR__GPIO_Port GPIOA
#define ADC_FET_DRAIN_F_B_Pin GPIO_PIN_7
#define ADC_FET_DRAIN_F_B_GPIO_Port GPIOA
#define MOTOR_DEF_IN2_Pin GPIO_PIN_4
#define MOTOR_DEF_IN2_GPIO_Port GPIOC
#define MOTOR_DEF_IN1_Pin GPIO_PIN_5
#define MOTOR_DEF_IN1_GPIO_Port GPIOC
#define ADC_BAT_ADC_Pin GPIO_PIN_0
#define ADC_BAT_ADC_GPIO_Port GPIOB
#define ADC_REF_5V_Pin GPIO_PIN_1
#define ADC_REF_5V_GPIO_Port GPIOB
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define MOTOR_TEMP_IN1_Pin GPIO_PIN_10
#define MOTOR_TEMP_IN1_GPIO_Port GPIOB
#define MOTOR_TEMP_IN2_Pin GPIO_PIN_11
#define MOTOR_TEMP_IN2_GPIO_Port GPIOB
#define MOTOR_FOOT_DUTY_Pin GPIO_PIN_6
#define MOTOR_FOOT_DUTY_GPIO_Port GPIOC
#define MOTOR_VENT_DUTY_Pin GPIO_PIN_7
#define MOTOR_VENT_DUTY_GPIO_Port GPIOC
#define PWR_ON_Pin GPIO_PIN_9
#define PWR_ON_GPIO_Port GPIOC
#define STB_CAN_Pin GPIO_PIN_12
#define STB_CAN_GPIO_Port GPIOC
#define RUN_LED_Pin GPIO_PIN_2
#define RUN_LED_GPIO_Port GPIOD
#define MOTOR_INTAKE_IN1_Pin GPIO_PIN_3
#define MOTOR_INTAKE_IN1_GPIO_Port GPIOB
#define MOTOR_INTAKE_IN2_Pin GPIO_PIN_4
#define MOTOR_INTAKE_IN2_GPIO_Port GPIOB
#define AC_MOTOR_DET_Pin GPIO_PIN_5
#define AC_MOTOR_DET_GPIO_Port GPIOB
#define BLOWER_PWM_Pin GPIO_PIN_6
#define BLOWER_PWM_GPIO_Port GPIOB
#define MOTOR_TEMP_DUTY_Pin GPIO_PIN_7
#define MOTOR_TEMP_DUTY_GPIO_Port GPIOB
#define MOTOR_DEF_DUTY_Pin GPIO_PIN_8
#define MOTOR_DEF_DUTY_GPIO_Port GPIOB
#define MOTOR_INTAKE_DUTY_Pin GPIO_PIN_9
#define MOTOR_INTAKE_DUTY_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

#define CAN_SYNC_MODE

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
