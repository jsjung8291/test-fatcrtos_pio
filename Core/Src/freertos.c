/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "cmsis_os.h"
#include "main.h"
#include "task.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iwdg.h"
#include "queue.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
extern WWDG_HandleTypeDef hwwdg;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart3;
extern TIM_HandleTypeDef htim4;
extern CAN_HandleTypeDef hcan;

extern uint8_t uart2_buffer[10]; // lin1 buffer
extern uint8_t uart2_buffer_len;
extern uint8_t uart3_buffer[10]; // lin2 buffer
extern uint8_t uart3_buffer_len;
extern uint8_t uart6_buffer[41]; // command buffer
extern uint8_t uart6_buffer_len;

extern uint8_t coolingFanDuty;
extern uint8_t isIGNON;

uint8_t modeAcuatatorVol = 0;
uint8_t tempAcuatatorVol = 0;
uint8_t intakeAcuatatorVol = 0;

uint8_t newModeAcuatatorVol = 0;
uint8_t newTempAcuatatorVol = 0;
uint8_t newIntakeAcuatatorVol = 0;
uint8_t newBlowerPwmDuty = 0;
uint16_t newCanRPMValue = 0;
uint16_t newCanFlag = 0;
uint8_t sendCanData = 0;

CAN_TxHeaderTypeDef TxHeader;
uint8_t TxData[8];
uint32_t TxMailbox;

ECOMP_t ecomp;

/* USER CODE END Variables */
osThreadId watchDogHandle;
osThreadId commandHandle;
osThreadId responseHandle;
osThreadId canCommHandle;
osThreadId hvacHandle;
osMessageQId commandQueueHandle;
osMessageQId responseQueueHandle;
osMessageQId canQueueHandle;
osMessageQId myQueue06Handle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */
uint8_t Lin_Checksum(uint8_t id, uint8_t data[]);
uint8_t Lin_CheckPID(uint8_t id);
/* USER CODE END FunctionPrototypes */

void watchDogTask(void const *argument);
void commandTask(void const *argument);
void responseTask(void const *argument);
void canCommTask(void const *argument);
void hvacTask(void const *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer,
                                   StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize);

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory(StaticTask_t **ppxIdleTaskTCBBuffer,
                                   StackType_t **ppxIdleTaskStackBuffer,
                                   uint32_t *pulIdleTaskStackSize) {
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of commandQueue */
  osMessageQDef(commandQueue, 5, MSG_t);
  commandQueueHandle = osMessageCreate(osMessageQ(commandQueue), NULL);

  /* definition and creation of responseQueue */
  osMessageQDef(responseQueue, 5, MSG_t);
  responseQueueHandle = osMessageCreate(osMessageQ(responseQueue), NULL);

  /* definition and creation of canQueue */
  osMessageQDef(canQueue, 5, MSG_t);
  canQueueHandle = osMessageCreate(osMessageQ(canQueue), NULL);

  /* definition and creation of myQueue06 */
  osMessageQDef(myQueue06, 5, MSG_t);
  myQueue06Handle = osMessageCreate(osMessageQ(myQueue06), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of watchDog */
  osThreadDef(watchDog, watchDogTask, osPriorityNormal, 0, 128);
  watchDogHandle = osThreadCreate(osThread(watchDog), NULL);

  /* definition and creation of command */
  osThreadDef(command, commandTask, osPriorityNormal, 0, 128);
  commandHandle = osThreadCreate(osThread(command), NULL);

  /* definition and creation of response */
  osThreadDef(response, responseTask, osPriorityNormal, 0, 128);
  responseHandle = osThreadCreate(osThread(response), NULL);

  /* definition and creation of canComm */
  osThreadDef(canComm, canCommTask, osPriorityNormal, 0, 128);
  canCommHandle = osThreadCreate(osThread(canComm), NULL);

  /* definition and creation of hvac */
  osThreadDef(hvac, hvacTask, osPriorityBelowNormal, 0, 128);
  hvacHandle = osThreadCreate(osThread(hvac), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */
}

/* USER CODE BEGIN Header_watchDogTask */
/**
 * @brief  Function implementing the watchDog thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_watchDogTask */
void watchDogTask(void const *argument) {
  /* USER CODE BEGIN watchDogTask */
  /* Infinite loop */
  for (;;) {
    HAL_IWDG_Refresh(&hiwdg);
    HAL_GPIO_TogglePin(RUN_LED_GPIO_Port, RUN_LED_Pin);
    osDelay(400);
  }
  /* USER CODE END watchDogTask */
}

/* USER CODE BEGIN Header_commandTask */
/**
 * @brief Function implementing the command thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_commandTask */
void commandTask(void const *argument) {
  /* USER CODE BEGIN commandTask */
  /* Infinite loop */
  char received[41];
  char id[3];
  int value = 0;
  int value1 = 0;
  int value2 = 0;
  int value3 = 0;
  int value4 = 0;
  while (1) {
    memset(received, 0, 41);
    if (xQueueReceive(commandQueueHandle, received, portMAX_DELAY) != pdTRUE) {
      printf("Error in Receiving from Queue\n");
    } else if (received[3] == ' ') {
      sscanf(received, "$%s %d\n", id, &value);
      if (strncmp(id, "CF", 2) == 0) {
        coolingFanDuty = (uint8_t)value;
        TIM4->CCR2 = coolingFanDuty;
      } else if (strncmp(id, "HV", 2) == 0) {
        sscanf(received, "$%s %d %d %d %d\n", id, &value1, &value2, &value3, &value4);
        newBlowerPwmDuty = value1;
        newModeAcuatatorVol = value2;
        newTempAcuatatorVol = value3;
        newIntakeAcuatatorVol = value4;
      } else if (strncmp(id, "EC", 2) == 0) {
        sscanf(received, "$%s %d %d\n", id, &value1, &value2);
        if (value2 != 9) {
          newCanRPMValue = value1;
          newCanFlag = value2;
          sendCanData = 1;
        } else {
          sendCanData = 0;
        }
      }
    }
  }
  /* USER CODE END commandTask */
}

/* USER CODE BEGIN Header_responseTask */
/**
 * @brief Function implementing the response thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_responseTask */
void responseTask(void const *argument) {
  /* USER CODE BEGIN responseTask */
  char strBuffer[41];
  for (;;) {
    if (xQueueReceive(responseQueueHandle, strBuffer, portMAX_DELAY) != pdTRUE) {
      printf("Error in Receiving from Queue\n");
    } else {
      HAL_UART_Transmit(&huart1, (uint8_t *)strBuffer, strlen(strBuffer), HAL_MAX_DELAY);
    }
  }
  /* USER CODE END responseTask */
}

/* USER CODE BEGIN Header_canCommTask */
/**
 * @brief Function implementing the canComm thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_canCommTask */
void canCommTask(void const *argument) {
  /* USER CODE BEGIN canCommTask */
  HAL_StatusTypeDef status;
  uint8_t CAN_ERR = 0;
  char strBuffer[41];
  static uint8_t CAN_ST = 1;
  /* Infinite loop */
  for (;;) {
    if (sendCanData == 0) {
      osDelay(10);
      continue;
    }
    TxHeader.StdId = 0x5A0; // Standard Identifier, 0 ~ 0x7FF
    TxHeader.ExtId = 0x00;  // Extended Identifier, 0 ~ 0x1FFFFFFF
    TxHeader.RTR = CAN_RTR_DATA;
    TxHeader.IDE = CAN_ID_STD;
    TxHeader.DLC = 8;
    TxHeader.TransmitGlobalTime = DISABLE;

    /* Set the data to be transmitted */
    memset(TxData, 0, 8);
    *(uint16_t *)&TxData[2] = newCanRPMValue;
    TxData[4] = newCanFlag;

#ifdef CAN_SYNC_MODE
    osThreadSuspend(canCommHandle);
#endif

    if (HAL_CAN_GetTxMailboxesFreeLevel(&hcan) != 0) {
      status = HAL_CAN_AddTxMessage(&hcan, &TxHeader, TxData, &TxMailbox);
      if (status != HAL_OK) {
        CAN_ERR = 1;
        osDelay(1000);
      }
      if (CAN_ERR == 1 && CAN_ST == 0) {
        sprintf(strBuffer, "&ER CAN\n");
        xQueueSend(responseQueueHandle, (void *)strBuffer, (TickType_t)30);
      } else if (CAN_ERR == 0 && CAN_ST == 1) {
        sprintf(strBuffer, "&OK CAN\n");
        xQueueSend(responseQueueHandle, (void *)strBuffer, (TickType_t)30);
      }
      CAN_ST = CAN_ERR;
    }
#ifndef CAN_SYNC_MODE
    osDelay(10);
#endif
  }
  /* USER CODE END canCommTask */
}

/* USER CODE BEGIN Header_hvacTask */
/**
 * @brief Function implementing the hvac thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_hvacTask */
void hvacTask(void const *argument) {
  /* USER CODE BEGIN hvacTask */
  /* Infinite loop */
  for (;;) {
    TIM4->CCR2 = newBlowerPwmDuty;

    if (abs(tempAcuatatorVol - newTempAcuatatorVol) < 10) {
      HAL_GPIO_WritePin(MOTOR_TEMP_IN1_GPIO_Port, MOTOR_TEMP_IN1_Pin, RESET);
      HAL_GPIO_WritePin(MOTOR_TEMP_IN2_GPIO_Port, MOTOR_TEMP_IN2_Pin, RESET);
    } else if (tempAcuatatorVol > newTempAcuatatorVol) {
      HAL_GPIO_WritePin(MOTOR_TEMP_IN1_GPIO_Port, MOTOR_TEMP_IN1_Pin, SET);
      HAL_GPIO_WritePin(MOTOR_TEMP_IN2_GPIO_Port, MOTOR_TEMP_IN2_Pin, RESET);
    } else if (tempAcuatatorVol < newTempAcuatatorVol) {
      HAL_GPIO_WritePin(MOTOR_TEMP_IN1_GPIO_Port, MOTOR_TEMP_IN1_Pin, RESET);
      HAL_GPIO_WritePin(MOTOR_TEMP_IN2_GPIO_Port, MOTOR_TEMP_IN2_Pin, SET);
    }

    // if (abs(modeAcuatatorVol - newModeAcuatatorVol) < 10) {
    //   HAL_GPIO_WritePin(MOTOR_MODE_IN1_GPIO_Port, MOTOR_MODE_IN1_Pin, RESET);
    //   HAL_GPIO_WritePin(MOTOR_MODE_IN2_GPIO_Port, MOTOR_MODE_IN2_Pin, RESET);
    // } else if (modeAcuatatorVol > newModeAcuatatorVol) {
    //   HAL_GPIO_WritePin(MOTOR_MODE_IN1_GPIO_Port, MOTOR_MODE_IN1_Pin, RESET);
    //   HAL_GPIO_WritePin(MOTOR_MODE_IN2_GPIO_Port, MOTOR_MODE_IN2_Pin, SET);
    // } else if (modeAcuatatorVol < newModeAcuatatorVol) {
    //   HAL_GPIO_WritePin(MOTOR_MODE_IN1_GPIO_Port, MOTOR_MODE_IN1_Pin, SET);
    //   HAL_GPIO_WritePin(MOTOR_MODE_IN2_GPIO_Port, MOTOR_MODE_IN2_Pin, RESET);
    // }

    if (abs(intakeAcuatatorVol - newIntakeAcuatatorVol) < 10) {
      HAL_GPIO_WritePin(MOTOR_INTAKE_IN1_GPIO_Port, MOTOR_INTAKE_IN1_Pin, RESET);
      HAL_GPIO_WritePin(MOTOR_INTAKE_IN2_GPIO_Port, MOTOR_INTAKE_IN2_Pin, RESET);
    } else if (intakeAcuatatorVol > newIntakeAcuatatorVol) {
      HAL_GPIO_WritePin(MOTOR_INTAKE_IN1_GPIO_Port, MOTOR_INTAKE_IN1_Pin, RESET);
      HAL_GPIO_WritePin(MOTOR_INTAKE_IN2_GPIO_Port, MOTOR_INTAKE_IN2_Pin, SET);
    } else if (intakeAcuatatorVol < newIntakeAcuatatorVol) {
      HAL_GPIO_WritePin(MOTOR_INTAKE_IN1_GPIO_Port, MOTOR_INTAKE_IN1_Pin, SET);
      HAL_GPIO_WritePin(MOTOR_INTAKE_IN2_GPIO_Port, MOTOR_INTAKE_IN2_Pin, RESET);
    }
    osDelay(50);
  }
  /* USER CODE END hvacTask */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
uint8_t Lin_Checksum(uint8_t id, uint8_t data[]) {
  uint8_t t;
  uint16_t sum;

  sum = data[0];
  if (id == 0x3c) {
    for (t = 1; t < 8; t++) {
      sum += data[t];
      if (sum & 0xff00) {
        sum &= 0x00ff;
        sum += 1;
      }
    }
    sum = ~sum;

    return (uint8_t)sum;
  }

  for (t = 1; t < 8; t++) {
    sum += data[t];
    if (sum & 0xff00) {
      sum &= 0x00ff;
      sum += 1;
    }
  }
  sum += Lin_CheckPID(id);
  if (sum & 0xff00) {
    sum &= 0x00ff;
    sum += 1;
  }
  sum = ~sum;
  return (uint8_t)sum;
}

uint8_t Lin_CheckPID(uint8_t id) {
  uint8_t returnpid;
  uint8_t P0;
  uint8_t P1;

  P0 = (((id) ^ (id >> 1) ^ (id >> 2) ^ (id >> 4)) & 0x01) << 6;
  P1 = ((~((id >> 1) ^ (id >> 3) ^ (id >> 4) ^ (id >> 5))) & 0x01) << 7;

  returnpid = id | P0 | P1;

  return returnpid;
}
/* USER CODE END Application */
